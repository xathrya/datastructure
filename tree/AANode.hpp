#ifndef XATH_DS_TREE_AA_H_
#define XATH_DS_TREE_AA_H_

/*************************************************************e**********
 *
 *  C++11 Compatible
 *
 **********************************************************************/

/*
 * AATree class
 *
 * Note: Type should be comparable.
 *
 * Operations:
 *   void insert(x)
 *   void remove(x)
 *   Comparable find(x)
 *   Comparable findMin(x)
 *   Comparable findMax(x)
 *   bool isEmpty()
 *   void makeEmpty()
 *   
 */

namespace xathrya {

template <class Comparable>
class AATree;

template <class Comparable>
class AANode {
    Comparable  element;
    AANode      *left   = nullptr;
    AANode      *right  = nullptr;
    int         level   = 1;
    
    AANode() { }
    AANode(const Comparable &e, AANode *lt, AANode *rt, int lv=1)
        : element(e), left(lt), right(rt), level(lv) { }
    
    friend class AATree<Comparable>;
};

template <class Comparable>
class AATree {
public:
    /** Constructors & Destructor **/
    AATree();                           /** Constructor **/
    AATree(const AATree &rhs);          /** Copy Constructor **/
                                        /** Move Constructor **/
    ~AATree();                          /** Destructor **/
    
    //------------------------------------------------------------------
    
    const Comparable & findMin() const;
    const Comparable & findMax() const;
    const Comparable & find(const Comparable & x) const;
    
    bool  is empty() const;
    void  makeEmpty() const;
    
    void  insert(const Comparable & x);
    void  remove(const Comparable & x);
    
    const AATree & operator=(const AATree & rhs);

private:    
    AANode<Comparable> *root;
    
    //-- Recursive ------------
    void insert(const Comparable & x, AANode<Comparable> * & t);
    void remove(const Comparable & x, AANode<Comparable> * & t);
    void makeEmpty(AANode<Comparable> * & t);
    
    //-- Rotations ------------
    void skew(AANode<Comparable> * & t) const;
    void split(AANode<Comparable> * & t) const;
    void rotateLeftChild(AANode<Comparable> * & t) const;
    void rotateRightChild(AANode<Comparable> * & t) const;
    AANode<Comparable> * clone(AANode<Comparable> * t) const;
};

/**
 * We have templates, so our implementation should be in same file
 * (can be logically or physically, but I choose physically)
 */

template <class Comparable>
AATree<Comparable>::AATree()
{
    root = nullptr;
}

/**
 * Copy Constructor
 */
template <class Comparable>
AATree<Comparable> :: AATree(const AATree<Comparable> & rhs)
{
    root = clone(rhs.root);
}



/**
 * Destructor
 */
template <class Comparable>
AATree<Comparable> :: ~AATree()
{
    makeEmpty();
}

/**
 * Insert X into the tree; duplicates are ignored
 */
template <class Comparable>
void AATree<Comparable> :: insert(const Comparable & x)
{
    insert(x, root);
}

/**
 * Remove x from the tree. If x is not found, nothing is done
 */
template <class Comparable>
void AATree<Comparable> :: remove(const Comparable & x)
{
    remove(x, root);
}

/**
 * Find the smallest item in the tree
 */
template <class Comparable>
const Comparable & AATree<Comparable> :: findMin() const 
{
    if (!isEmpty()) {
        AANode<Comparable> *ptr = root;

        while (ptr->left != nullptr)
            ptr = ptr->left;

        return ptr->element;
    }
}

/**
 * Find the largest item in the tree
 */
template <class Comparable>
const Comparable & AATree<Comparable> :: findMax() const
{
    if (!isEmpty()) {
        AANode<Comparable> *ptr = root;

        while (ptr->right != nullptr)
            ptr = ptr->right;

        return ptr->element;
    }
}

/**
 * Find item x in the tree
 */
template <class Comparable>
const Comparable & 
AATree<Comparable> :: find(const Comparable & x) const 
{
    AANode<Comparable> *current = root;
    
    while (root != nullptr) {
        if (x < current->element)
            current = current->left;
        else if (x > current->element)
            current = current->right;
        else 
            return current->element;
    }
}

/**
 * Make tree empty
 */
template <class Comparable>
void AATree<Comparable> :: makeEmpty()
{
    makeEmpty(root);
}

/**
 * Test if tree is empty
 */
template <class Comparable>
bool AATree<Comparable> :: isEmpty()
{
    return (root == nullptr);
}

/**
 * Deep copy
 */
template <class Comparable>
const Comparable &
AATree<Comparable> :: operator=(const AATree<Comparable> & rhs)
{
    if (this != &rhs) {
        makeEmpty();
        root = clone(rhs.root);
    }
    return *this;
}

//-- Internal Methods ----------------

/**
 * Internal method for item insertion
 */
template <class Comparable>
void AATree<Comparable> ::
insert(const Comparable & x, AANode<Comparable> * & t)
{
    if (t == nullptr)
        t = new AANode<Comparable>(x, nullptr, nullptr);
    else if (x < t->element)
        insert(x, t->left);
    else if (x > t->element)
        insert(x, t->right);
    else
        return;     // duplicate
    
    skew(t);
    split(t);
}

/**
 * Internal method for item removal
 */
template <class Comparable>
void AATree<Comparable> ::
remove(const Comparable & x, AANode<Comparable> * & t)
{
    static AANode<Comparable>   *lastNode,
                                *deletedNode = nullptr;
    if (t != nullptr) {
        //-- Step 1: Search down the tree and set lastNode and deletedNode
        lastNode = t;
        if (x < t->element)
            remove(x, t->left);
        else {
            deletedNode = t;
            remove(x, t->right);
        }
        
        //-- Step 2: If at the bottom of the tree and x is present, remove
        if (t == lastNode) {
            if ((deletedNode == nullptr) || (x != deletedNode->element))
                return; // Item not found
            deletedNode->element = t->element;
            deletedNode = nullptr;
            t = t->right;
            delete lastNode;
        }
        
        //-- Step 3: Otherwise, we are not at the bottom
        else {
            if ((t->left->level < t->level -1) ||
                (t->right->level < t->level -1))
            {
                if (t->right->level > --t->level)
                    t->right->level = t->level;
                skew(t);
                skew(t->right);
                skew(t->right->right);
                split(t);
                split(t->right);
            }
        }
    }
}

/**
 * Internal method for make subtree empty
 */
template <class Comparable>
void AATree<Comparable> :: makeEmpty(AANode<Comparable> * & t)
{
    if (t != nullptr) {
        makeEmpty(t->left);
        makeEmpty(t->right);
        delete t;
    }
    t = nullptr;
}

/**
 * Rotate binary tree node with left child.
 */
template <class Comparable>
void AATree<Comparable> :: rotateLeftChild(AANode<Comparable> * & k2) const
{
    AANode<Comparable> *k1 = k2->left;
    k2->left = k1->right;
    k1->right = k2;
    k2 = k1;
}

/**
 * Rotate binary tree node with right child
 */
template <class Comparable>
void AATree<Comparable> :: rotateRightChild(AANode<Comparable> * & k1) const
{
    AANode<Comparable> *k2 = k1->right;
    k1->right = k2->left;
    k2->left = k1;
    k1 = k2;
}

/**
 * Skew primitive for AA-trees
 * t is the node that roots the tree.
 */
template <class Comparable>
void AATree<Comparable> :: skew(AANode<Comparable> * & t) const
{
    if (t->left->level == t->level)
        rotateLeftChild(t);
}

/**
 * Split primitive for AA-trees
 * t is the node that roots the tree
 */
template <class Comparable>
void AATree<Comparable> :: split(AANode<Comparable> * & t) const
{
    if (t->right->right->level == t->level) {
        rotateRightChild(t);
        t->level++;
    }
}

/**
 * Internal method to clone subtree
 */
template <class Comparable>
AANode<Comparable> *
AATree<Comparable> :: clone(AANode<Comparable> * t) const
{
    if (t == nullptr)
        return nullptr;
    else 
        return new AANode<Comparable>(  t->element, clone(t->left),
                                        clone(t->right), t->level);
}

}

#endif
