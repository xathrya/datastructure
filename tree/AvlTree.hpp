#ifndef XATH_DS_TREE_AVL_H_
#define XATH_DS_TREE_AVL_H_

/*************************************************************e**********
 *
 *  C++11 Compatible
 *
 **********************************************************************/

/*
 * AvlTree class
 *
 * Note: Type should be comparable.
 *
 * Operations:
 *   void insert(x)
 *   void remove(x)
 *   Comparable find(x)
 *   Comparable findMin(x)
 *   Comparable findMax(x)
 *   bool isEmpty()
 *   void makeEmpty()
 *   
 */

namespace xathrya {

template <class Comparable>
class AvlTree;

template <class Comparable>
class AvlNode {
    Comparable  element;
    AvlNode      *left   = nullptr;
    AvlNode      *right  = nullptr;
    int         height   = 0;
    
    AvlNode() { }
    AvlNode(const Comparable &e, AvlNode *lt, AvlNode *rt, int h=1)
        : element(e), left(lt), right(rt), height(h) { }
    
    friend class AvlTree<Comparable>;
};

template <class Comparable>
class AvlTree {
public:
    /** Constructors & Destructor **/
    AvlTree();                              /** Constructor **/
    AvlTree(const AvlTree &rhs);            /** Copy Constructor **/
    AvlTree(AvlTree &&rhs);                 /** Move Constructor **/
    ~AvlTree();                             /** Destructor **/
    
    //------------------------------------------------------------------
    
    bool findMin(Comparable & x) const;     
    bool findMax(Comparable & x) const;
    bool find(const Comparable & x) const;
    
    bool  isEmpty() const;
    void  makeEmpty();
    
    void  insert(const Comparable & x);
    void  remove(const Comparable & x);
    
    const AvlTree & operator=(const AvlTree & rhs);     //-- Clone
    AvlTree & operator<<(const Comparable & x);         //-- Insertion

private:    
    AvlNode<Comparable> *root;
    
    //-- Recursive ------------
    void insert(const Comparable & x, AvlNode<Comparable> * & t);
    void remove(const Comparable & x, AvlNode<Comparable> * & t);
    void makeEmpty(AvlNode<Comparable> * & t);
    AvlNode<Comparable> * clone(AvlNode<Comparable> *t) const;
    
    //-- Avl Manipulations ------------
    int height(AvlNode<Comparable> *t) const;
    int max(int lhs, int rhs) const;
    void rotateLeftChild(AvlNode<Comparable> * & k2) const;
    void rotateRightChild(AvlNode<Comparable> * & k1) const;
    void doubleLeftChild(AvlNode<Comparable> * & k3) const;
    void doubleRightChild(AvlNode<Comparable> * & k1) const;
};

/**
 * We have templates, so our implementation should be in same file
 * (can be logically or physically, but I choose physically)
 */

template <class Comparable>
AvlTree<Comparable>::AvlTree()
{
    root = nullptr;
}

/**
 * Copy Constructor
 */
template <class Comparable>
AvlTree<Comparable> :: AvlTree(const AvlTree<Comparable> & rhs)
{
    root = clone(rhs.root);
}

/**
 * Move Constructor
 */
template <class Comparable>
AvlTree<Comparable> :: AvlTree(AvlTree<Comparable> && rhs)
{
    root = rhs.root;
    rhs.root = nullptr;
}

/**
 * Destructor
 */
template <class Comparable>
AvlTree<Comparable> :: ~AvlTree()
{
    makeEmpty();
}

/**
 * Insert X into the tree; duplicates are ignored
 */
template <class Comparable>
void AvlTree<Comparable> :: insert(const Comparable & x)
{
    insert(x, root);
}

/**
 * Remove x from the tree. If x is not found, nothing is done
 */
template <class Comparable>
void AvlTree<Comparable> :: remove(const Comparable & x)
{
    remove(x, root);
}

/**
 * Find the smallest item in the tree
 */
template <class Comparable>
bool AvlTree<Comparable> :: findMin(Comparable & x) const 
{
    if (isEmpty())
        return false;

    AvlNode<Comparable> *ptr = root;

    while (ptr->left != nullptr)
        ptr = ptr->left;

    x = ptr->element;
    return true;
}

/**
 * Find the largest item in the tree
 */
template <class Comparable>
bool AvlTree<Comparable> :: findMax(Comparable &x) const
{
    if (isEmpty())
        return false;
    
    AvlNode<Comparable> *ptr = root;
    while (ptr->right != nullptr)
        ptr = ptr->right;

    x = ptr->element;
    return true;
}

/**
 * Find item x in the tree
 */
template <class Comparable>
bool AvlTree<Comparable> :: find(const Comparable & x) const 
{
    AvlNode<Comparable> *current = root;
    
    while (current != nullptr) {
        if (x < current->element)
            current = current->left;
        else if (x > current->element)
            current = current->right;
        else 
            break;
    }
    return (current != nullptr);
}

/**
 * Make tree empty
 */
template <class Comparable>
void AvlTree<Comparable> :: makeEmpty()
{
    makeEmpty(root);
}

/**
 * Test if tree is empty
 */
template <class Comparable>
bool AvlTree<Comparable> :: isEmpty() const
{
    return (root == nullptr);
}

/**
 * Deep copy
 */
template <class Comparable>
const AvlTree<Comparable> &
AvlTree<Comparable> :: operator=(const AvlTree<Comparable> & rhs)
{
    if (this != &rhs) {
        makeEmpty();
        root = clone(rhs.root);
    }
    return *this;
}

/**
 * Insertion an element
 */
template <class Comparable>
AvlTree<Comparable> &
AvlTree<Comparable> :: operator<<(const Comparable & x)
{
    insert(x, root);
    return *this;
}

//-- Internal Methods ----------------

/**
 * Internal method for item insertion
 */
template <class Comparable>
void AvlTree<Comparable> ::
insert(const Comparable & x, AvlNode<Comparable> * & t)
{
    if (t == nullptr)
        t = new AvlNode<Comparable>(x, nullptr, nullptr);
    else if (x < t->element) {
        insert(x, t->left);
        if (height(t->left) - height(t->right) == 2) {
            if (x < t->left->element)
                rotateLeftChild(t);
            else
                doubleLeftChild(t);
        }
    } else if (x > t->element) {
        insert(x, t->right);
        if (height(t->right) - height(t->left) == 2) {
            if (t->right->element < x)
                rotateRightChild(t);
            else
                doubleRightChild(t);
        }
    } else
        return;     // duplicate
    
    t->height = max(height(t->left), height(t->right)) + 1;
}

/**
 * Internal method for item removal
 */
template <class Comparable>
void AvlTree<Comparable> ::
remove(const Comparable & x, AvlNode<Comparable> * & t)
{
    static AvlNode<Comparable>   *lastNode,
                                *deletedNode = nullptr;
    if (t != nullptr) {
        //-- Step 1: Search down the tree and set lastNode and deletedNode
        lastNode = t;
        if (x < t->element)
            remove(x, t->left);
        else {
            deletedNode = t;
            remove(x, t->right);
        }
        
        //-- Step 2: If at the bottom of the tree and x is present, remove
        if (t == lastNode) {
            if ((deletedNode == nullptr) || (x != deletedNode->element))
                return; // Item not found
            deletedNode->element = t->element;
            deletedNode = nullptr;
            t = t->right;
            delete lastNode;
        }
        
        //-- Step 3: Otherwise, we are not at the bottom
        else {
            if ((t->left->height < t->height -1) ||
                (t->right->height < t->height -1))
            {
                if (t->right->height > --t->height)
                    t->right->height = t->height;
                skew(t);
                skew(t->right);
                skew(t->right->right);
                split(t);
                split(t->right);
            }
        }
    }
}

/**
 * Internal method for make subtree empty
 */
template <class Comparable>
void AvlTree<Comparable> :: makeEmpty(AvlNode<Comparable> * & t)
{
    if (t != nullptr) {
        makeEmpty(t->left);
        makeEmpty(t->right);
        delete t;
    }
    t = nullptr;
}

/**
 * Internal method to clone subtree
 */
template <class Comparable>
AvlNode<Comparable> *
AvlTree<Comparable> :: clone(AvlNode<Comparable> * t) const
{
    if (t == nullptr)
        return nullptr;
    else 
        return new AvlNode<Comparable>(  t->element, clone(t->left),
                                        clone(t->right), t->height);
}

/**
 * Return the height of node t or -1 if nullptr
 */
template <class Comparable>
int AvlTree<Comparable> :: height(AvlNode<Comparable> *t) const
{
    return ( (t == nullptr) ? -1 : t->height );
}

/**
 * Return maximum of lhs and rhs
 */
template <class Comparable>
int AvlTree<Comparable> :: max(int lhs, int rhs) const
{
    return ( (lhs > rhs) ? lhs : rhs );
}

/**
 * Rotate binary tree node with left child.
 * For AVL trees, this is a single rotation for case 1
 * Update heights, then set new root
 */
template <class Comparable>
void AvlTree<Comparable> :: rotateLeftChild(AvlNode<Comparable> * & k2) const
{
    AvlNode<Comparable> *k1 = k2->left;
    k2->left = k1->right;
    k1->right = k2;
    k2->height = max(height(k2->left), height(k2->right)) + 1;
    k1->height = max(height(k1->left), k2->height) + 1;
    k2 = k1;
}

/**
 * Rotate binary tree node with right child
 * For AVL trees, this is asingle rotation for case 4
 * Update heights, then set new root.
 */
template <class Comparable>
void AvlTree<Comparable> :: rotateRightChild(AvlNode<Comparable> * & k1) const
{
    AvlNode<Comparable> *k2 = k1->right;
    k1->right = k2->left;
    k2->left = k1;
    k1->height = max(height(k1->left), height(k1->right)) + 1;
    k2->height = max(height(k2->left), k1->height) + 1;
    k1 = k2;
}

/**
 * Double rotates binary tree node; first left child
 * with its right child; then node k3 with new left child
 * For AVL trees, this is a double rotation for case 2.
 * Update heights, then set new root.
 */
template <class Comparable>
void AvlTree<Comparable> :: doubleLeftChild(AvlNode<Comparable> * & k3) const
{
    rotateRightChild(k3->left);
    rotateLeftChild(k3);
}

/**
 * Double rotates binary tree node; first right child
 * with its left child; then node k1 with new right child
 * For AVL trees, this is a double rotation for case 3.
 * Update heights, then set new root.
 */
template <class Comparable>
void AvlTree<Comparable> :: doubleRightChild(AvlNode<Comparable> * & k1) const
{
    rotateLeftChild(k1->right);
    rotateRightChild(k1);
}

}

#endif
