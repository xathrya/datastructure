#ifndef XATH_DS_EXCEPTIONS_H_
#define XATH_DS_EXCEPTIONS_H_

class Underflow { };
class Overflow { };
class OutOfMemory { };
class BadIterator { };

#endif
